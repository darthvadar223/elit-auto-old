$("#leave-benefit").click(function () {
    $("body").css("overflow","hidden");
    $("#benefit-overlay")
        .css("visibility", "visible")
        .css("opacity", "1")
        .children("div")
        .css("transform", "scale(1)");
});

function benefit_overlay_close() {
    $("body").css("overflow","auto");
    $("#benefit-overlay")
        .css("opacity", "0")
        .children("div")
        .css("transform", "scale(0)");

    setTimeout(function () {
        $("#benefit-overlay")
            .css("visibility", "hidden")
    }, 300);
}

$("#benefit-overlay>div>div>img")
    .click(function () {
        benefit_overlay_close();
    });

$("#benefit-form>.estimate>.stars>img").mouseenter(function () {
    $fill_count = $(this).index();
    for($i = 0; $i < 5; $i++) {
        if($fill_count >= $i) {
            $($("#benefit-form>.estimate>.stars>img")[$i]).addClass("hovered");
        } else {
            $($("#benefit-form>.estimate>.stars>img")[$i]).removeClass("hovered");
        }
    }
});

$("#benefit-form>.estimate>.stars>img").mouseleave(function () {
    $(".hovered").removeClass("hovered");
});

bind();
function bind() {
    $("#benefit-form>.estimate>.stars>img").click(function () {
        $("#benefit-form").children("span").css("visibility", "hidden");
        $(this).parent().siblings("label").css("color", "#0bca95");
        $fill_count = $(this).index();
        $($("#benefit-form>.estimate>input")).val($fill_count);
        $stars = "";
        for($i = 0; $i < 5; $i++) {
            if($fill_count >= $i) {
                $stars += `<img src="img/star.svg" alt="звезда">`;
            } else {
                $stars += `<img src="img/star-empty.svg" alt="пустая звезда">`;
            }
        }
        $(this).parent().addClass("selected").html($stars);
        bind();
    });
}

$("#benefit-form>.file>input").change(function () {
    if(this.files.length > 0) {
        $(this).parent().children("div").text(this.files[0].name);
    } else {
        $(this).parent().children("div").text("Прикрепить фото");
    }
});

$("#benefit-form input").focus(function () {
    $("#benefit-form").children("span").css("visibility", "hidden");
    $(this)
        .css("border-color", "#0bca95");
});

$("#benefit-form input").focusout(function () {
    if($(this).val() === "") {
        $(this)
            .css("border-color", "#d90606")
            .siblings("label")
            .css("color", "#d90606");
    } else {
        $(this)
            .css("border-color", "#0bca95")
            .siblings("label")
            .css("color", "#0bca95");
    }
});

$("#benefit-form").submit(function (e) {
    $length = $("#benefit-form input").length - 2;
    $filled_fields = true;
    for($i = 0; $i < $length; $i++) {
        if($($("#benefit-form input")[$i]).val() === "") {
            if($filled_fields)
                $filled_fields = false;
            $($("#benefit-form input")[$i])
                .css("border-color", "#d90606")
                .siblings("label")
                .css("color", "#d90606");
        }
    }
    if($filled_fields) {
        $data = $(this).serialize();
        console.log($data);
        $.ajax({
            url: "hz.php",
            type: "POST",
            data: $data,
            success: function (data) {
                // $(".form").children(".form-default").remove();
                // $(".form")
                //     .append(`
                //         <div class="form-success">
                //             <img src="img/form/success.svg" alt="успех">
                //             <h1 class="success">Заявка отправлена</h1>
                //             <div>
                //                 <p>Ожидайте нашего ответа.</p>
                //                 <p>Мы свяжемся с Вами в ближайшее время.</p>
                //             </div>
                //         </div>
                // `);
            },
            error: function (data) {
                // $(".form").children(".form-default").remove();
                // $(".form")
                //     .append(`
                //         <div class="form-error">
                //             <img src="img/form/database.svg" alt="ошибка">
                //             <h1 class="error">Упс… на сервере ошибка</h1>
                //             <div>
                //                 <p>Произошла ошибка на сервере.</p>
                //                 <p><a href="#" id="refresh">Обновите</a> страницу и заполните
                //                     форму заново</p>
                //             </div>
                //         </div>`
                //     );
            },
            complete: function () {
                // $("#refresh").click(function () {
                //     location.reload();
                // });
            }
        });
    } else {
        $(this).children("span").css("visibility", "visible");
    }
});