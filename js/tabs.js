$(".tab-head").click(function (e) {
    $(".active-tab").children("div").children(".content").slideToggle(500);
    $hasclass = $(this).parent().parent().hasClass("active-tab");
    $(".active-tab").removeClass("active-tab");
    if(!$hasclass) {
        $(this).parent().parent().addClass("active-tab");
        $(this).parent().children(".content").slideToggle(500);
    }
})